﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gomoku
{
    public partial class Form1 : Form
    {
        Board myBoard = new Board();
        private PiecesType nextPiecesType = PiecesType.BLACK;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            BlackPieces temp = new BlackPieces(e.X, e.Y);
            if (temp.findNodeX() != -1 && temp.findNodeY() != -1)
            {
                if (nextPiecesType == PiecesType.BLACK)
                {
                    //this.Controls.Add(new BlackPieces(e.X, e.Y));
                    if(myBoard.setBoardStatus(temp.findNodeX(), temp.findNodeY(), (int)nextPiecesType))
                    {
                        this.Controls.Add(new BlackPieces(temp.realX(temp.findNodeX()), temp.realY(temp.findNodeY())));
                        if (myBoard.isWin(temp.findNodeX(), temp.findNodeY(), (int)nextPiecesType))
                        {
                            MessageBox.Show("黑勝");
                            Application.Exit();
                        }
                        nextPiecesType = PiecesType.WHITE;
                    }
                }
                else if (nextPiecesType == PiecesType.WHITE)
                {
                    //this.Controls.Add(new WhitePieces(e.X, e.Y));
                    if(myBoard.setBoardStatus(temp.findNodeX(), temp.findNodeY(), (int)nextPiecesType))
                    {
                        this.Controls.Add(new WhitePieces(temp.realX(temp.findNodeX()), temp.realY(temp.findNodeY())));
                        if (myBoard.isWin(temp.findNodeX(), temp.findNodeY(), (int)nextPiecesType))
                        { 
                            MessageBox.Show("白勝");
                            Application.Exit();
                        }
                        nextPiecesType = PiecesType.BLACK;
                    }
                }
            }
            //MessageBox.Show(myBoard.printBoardStatus());
        }
    }
}
