﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gomoku
{
    class Board
    {
        private const int BOARD_GRID = 19;
        private int[,] boardStatus = new int [BOARD_GRID, BOARD_GRID];
        public Board()
        {
            for(int i = 0; i < BOARD_GRID; i++)
            {
                for (int j = 0; j < BOARD_GRID; j++)
                {
                    boardStatus[i, j] = -1;
                }
            }
        }

        public bool setBoardStatus(int x, int y, int type)
        {
            if (boardStatus[y, x] != -1)
                return false;
            boardStatus[y, x] = type;
            return true;
        }

        public bool isWin(int x, int y, int type)
        {
            if (sameTypeNumber(x, y, type, Direction.N) + sameTypeNumber(x, y, type, Direction.S) >= 4)
                return true;
            if (sameTypeNumber(x, y, type, Direction.EN) + sameTypeNumber(x, y, type, Direction.WS) >= 4)
                return true;
            if (sameTypeNumber(x, y, type, Direction.E) + sameTypeNumber(x, y, type, Direction.W) >= 4)
                return true;
            if (sameTypeNumber(x, y, type, Direction.ES) + sameTypeNumber(x, y, type, Direction.WN) >= 4)
                return true;
            return false;
        }

        public int sameTypeNumber(int x, int y, int type, Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    if (x == 0)
                        return 0;
                    else if (boardStatus[y, x - 1] == type)
                        return 1 + sameTypeNumber(x - 1, y, type, Direction.N);
                    break;
                case Direction.EN:
                    if (x == 0 || y == BOARD_GRID - 1)
                        return 0;
                    else if (boardStatus[y + 1, x - 1] == type)
                        return 1 + sameTypeNumber(x - 1, y + 1, type, Direction.EN);
                    break;
                case Direction.E:
                    if (y == BOARD_GRID - 1)
                        return 0;
                    else if (boardStatus[y + 1, x] == type)
                        return 1 + sameTypeNumber(x, y + 1, type, Direction.E);
                    break;
                case Direction.ES:
                    if (x == BOARD_GRID - 1 || y == BOARD_GRID - 1)
                        return 0;
                    else if (boardStatus[y + 1, x + 1] == type)
                        return 1 + sameTypeNumber(x + 1, y + 1, type, Direction.ES);
                    break;
                case Direction.S:
                    if (x == BOARD_GRID - 1)
                        return 0;
                    else if (boardStatus[y, x + 1] == type)
                        return 1 + sameTypeNumber(x + 1, y, type, Direction.S);
                    break;
                case Direction.WS:
                    if (x == BOARD_GRID - 1 || y == 0)
                        return 0;
                    else if (boardStatus[y - 1, x + 1] == type)
                        return 1 + sameTypeNumber(x + 1, y - 1, type, Direction.WS);
                    break;
                case Direction.W:
                    if (y == 0)
                        return 0;
                    else if (boardStatus[y - 1, x] == type)
                        return 1 + sameTypeNumber(x, y - 1, type, Direction.W);
                    break;
                case Direction.WN:
                    if (y == 0 || x == 0)
                        return 0;
                    else if (boardStatus[y - 1, x - 1] == type)
                        return 1 + sameTypeNumber(x - 1, y - 1, type, Direction.WN);
                    break;
                default:
                    break;
            }
            return 0;
        }

        public String printBoardStatus()
        {
            String temp = "";
            for(int i = 0; i < BOARD_GRID; i++)
            {
                for (int j = 0; j < BOARD_GRID; j++)
                {
                    temp += boardStatus[i, j] + "|";
                }
                temp += "\n";
            }
            return temp;
        }
    }
}
