﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gomoku
{
    enum PiecesType
    {
        BLACK, WHITE
    }

    enum Direction
    {
        N, EN, E, ES, S, WS, W, WN
    }
}
