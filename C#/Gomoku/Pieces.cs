﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Gomoku
{
    abstract class Pieces : PictureBox
    {
        private int x;
        private int y;
        private static readonly int PIECES_SIZE = 30;
        private static readonly int EFFECTIVE_SIZE = 10;
        private static readonly int GRID_X = 34;
        private static readonly int GRID_Y = 34;
        private static readonly int BOADER_X = 20;
        private static readonly int BOADER_Y = 22;

        public Pieces (int x, int y)
        {
            this.x = x;
            this.y = y;
            this.Size = new Size(PIECES_SIZE, PIECES_SIZE);
            this.BackColor = Color.Transparent;
            this.Location = new Point(x - PIECES_SIZE/2, y - PIECES_SIZE/2);
            this.SizeMode = PictureBoxSizeMode.Zoom;
        }

        public int findNodeX ()
        {
            int val = this.x;
            int remainder = (val - BOADER_X) % GRID_X;
            if (remainder < EFFECTIVE_SIZE)
                return (val - BOADER_X) / GRID_X;
            else if (remainder > (GRID_X - EFFECTIVE_SIZE))
                return (val - BOADER_X) / GRID_X + 1;
            else
                return -1; 
        }
        public int findNodeY()
        {
            int val = this.y;
            int remainder = (val - BOADER_Y) % GRID_Y;
            if (remainder < EFFECTIVE_SIZE)
                return (val - BOADER_Y) / GRID_Y;
            else if (remainder > (GRID_Y - EFFECTIVE_SIZE))
                return (val - BOADER_Y) / GRID_Y + 1;
            else
                return -1;
        }

        public int realX(int nodeX)
        {
            return nodeX * GRID_X + BOADER_X;
        }
        public int realY(int nodeY)
        {
            return nodeY * GRID_Y + BOADER_Y;
        }
    }
}
