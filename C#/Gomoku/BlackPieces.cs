﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Gomoku
{
    class BlackPieces : Pieces
    {
        public BlackPieces(int x, int y) : base (x, y)
        {
            this.Image = Properties.Resources.black;
        }
    }
}
